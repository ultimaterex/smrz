Serubii's MRZ Library


[![](https://jitpack.io/v/com.gitlab.ultimaterex/smrz.svg)](https://jitpack.io/#com.gitlab.ultimaterex/smrz)




To use this library, add this to your project gradle
```gradle
allprojects {
    repositories {
         ...
        maven {
            url 'https://www.jitpack.io'
        }
    }
}
```


then you can get the pom from [Jitpack](https://jitpack.io/#com.serubii/smrz)