package com.serubii.smrz

import com.serubii.smrz.constants.invalidMRZ
import com.serubii.smrz.constants.validTD1
import com.serubii.smrz.constants.validTD2
import com.serubii.smrz.constants.validTD3
import com.serubii.smrz.helpers.MRZValidator
import org.junit.Assert.fail
import org.junit.Test

/**
 * local unit tests, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class MRZValidatorUnitTest {

    private val mrzv = MRZValidator()

    @Test
    fun validateTD1Block() {
        mrzv.validateTD1Block(validTD1)
    }

    @Test
    fun validateTD2Block() {
        mrzv.validateTD2Block(validTD2)

    }

    @Test
    fun validateTD3Block() {
        mrzv.validateTD3Block(validTD3)
    }

    @Test
    fun processInvalidTD1() {
        if (!mrzv.validateTD1Block(invalidMRZ)) {
            // success!
            println("Successfully failed to create a TD1 MRZ")
        } else { // should have failed
            fail("Failed to fail the validity test")
        }
    }

    @Test
    fun processInvalidTD2() {
        if (!mrzv.validateTD2Block(invalidMRZ)) {
            // success!
            println("Successfully failed to create a TD2 MRZ")
        } else { // should have failed
            fail("Failed to fail the validity test")
        }
    }

    @Test
    fun processInvalidTD3() {
        if (!mrzv.validateTD3Block(validTD3)) {
            // success!
            println("Successfully failed to create a TD3 MRZ")
        } else { // should have failed
            fail("Failed to fail the validity test")
        }
    }


}