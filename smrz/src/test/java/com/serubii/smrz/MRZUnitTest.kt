package com.serubii.smrz

import com.serubii.smrz.constants.invalidMRZ
import com.serubii.smrz.constants.validTD1
import com.serubii.smrz.constants.validTD2
import com.serubii.smrz.constants.validTD3
import org.junit.Assert.fail
import org.junit.Test

/**
 * local unit tests, which will execute on the development machine (host).
*
* See [testing documentation](http://d.android.com/tools/testing).
*/
class MRZUnitTest {

    @Test
    fun createInvalidMRZ() {
        try {
            MRZ(invalidMRZ) // should throw InvalidMRZCodeException
            fail("Should have thrown a InvalidMRZCode Exception")
        } catch (e: InvalidMRZCodeException) {
            // success!
            println("Successfully failed to create a MRZ")
        }
    }

    @Test
    fun createValidTD1() {
        val x = MRZ(validTD1)
        println("Successfully created a " + x.getMRZType())
    }

    @Test
    fun createValidTD2() {
        val x = MRZ(validTD2)
        println("Successfully created a " + x.getMRZType())
    }

    @Test
    fun createValidTD3() {
        val x = MRZ(validTD3)
        println("Successfully created a " + x.getMRZType())

    }


}
