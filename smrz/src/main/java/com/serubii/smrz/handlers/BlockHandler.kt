package com.serubii.smrz.handlers

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.serubii.smrz.MRZ
import com.serubii.smrz.helpers.MRZValidator
import com.serubii.smrz.helpers.OCRFixer
import com.serubii.smrz.others.stripWhiteSpace


/**
 * A class that processes MRZ types from OCR Blocks
 *
 * use with [process] after calling OCR
 *
 * TD1 3x30
 * TD2 2x36
 * TD3 2x44
 *
 * data accessible from [HashMap]<[String], [String]> : with Keys available in SConstants or->
 * * ID_TYPE
 * * ISSUE_COUNTRY
 * * DOCUMENT_NUMBER
 * * EXPIRATION
 * * SURNAME
 * * GIVEN_NAME
 * * GENDER
 * * DOB
 * * NATIONALITY
 * * OPTIONAL_INFO
 * * MRZ
 *
 *
 *
 * @author Rex Serubii
 */
class BlockHandler(ctx: Context) {
    private val TAG = this.javaClass.simpleName
    private val context: Context

    private val ocrf = OCRFixer()
    private val mrzv = MRZValidator()


    private var supportTD1 :Boolean = true
    private var supportTD2 :Boolean = true
    private var supportTD3 :Boolean = true
    private var legacy : Boolean = true

    private val supportVal: Int


    init {
        supportVal = checkSettings()
        context = ctx
    }

    /**
     * Check what modes are supported run this to check against saved values
     * @return Int
     */
    private fun checkSettings(): Int {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        supportTD1 = sharedPreferences.getBoolean("TD1", true)
        supportTD2 = sharedPreferences.getBoolean("TD2", true)
        supportTD3 = sharedPreferences.getBoolean("TD3", true)
        legacy = sharedPreferences.getBoolean("Legacy", false)

        // check for deprecation
        if (legacy && supportTD1 && supportTD2 && supportTD3) {
            // Full Support (11) delegate to legacy handler
            return 11
        }
        else if (supportTD1 && supportTD2 && supportTD3) {
            // Standard Support TD1, TD2, TD3 (10)
            return 10
        }
        else if (supportTD1 && supportTD2) {
            // Only TD1 and TD2 Support (9)
            return 9
        }
        else if (supportTD1 && supportTD3) {
            // Only TD1 and TD3 Support (8)
            return 8
        }
        else if (supportTD2 && supportTD3) {
            //  Only TD2 and TD3 Support (7)
            return 7
        }
        else if (supportTD1) {
            // Only TD1 Support (6)
            return 6
        }
        else if (supportTD2) {
            // Only TD2 Support (5)
            return 5
        }
        else if (supportTD3) {
            // Only TD3 Support (4)
            return 4
        }

        Log.wtf(TAG, "Did you forget to enable any of the supported formats?")
        return 0
    }

    /**
     * processes block data returned from OCR
     * @param text_by_block ArrayList<String>
     * @return MRZ?
     */
    fun process(text_by_block: ArrayList<String>): MRZ? {

        val mrz = checkBlockCompatibility(text_by_block)
        if (mrz != null) {
            return MRZ(mrz)
        }
        return null
    }

    /**
     *  Checks Compatibility for standard feature set
     * @param blocks ArrayList<String>
     * @return String?
     */
    private fun checkBlockCompatibility(blocks: ArrayList<String>): String? {
        val blockList = ArrayList<String>()
        var MRZ = ""


        // filter valid MRZ types
        for (block in blocks) {
            if (block.startsWith("A") ||
                block.startsWith("C") ||
                block.startsWith("I") ||
                block.startsWith("P") ||
                block.startsWith("V")) {
                blockList.add(block)
            } else {
                Log.w(TAG, "Rejected Block: [$block] because of start")
            }
        }


        if (blockList.size == 1) {
            // valid MRZ is 3lines x 30character with size of 90
            // valid MRZ is 2 lines x 44char/36char with size of 88/72
            var block = stripWhiteSpace(blockList[0])
            block = ocrf.fixExperimental(block)
            when (block.length) {
                //TD1
                90 -> {
                    if (!supportTD1) {
                        return null
                    }
                    val mrz = ocrf.fixTD1(block)
                    // handle validation checker
                    return if (mrzv.validateTD1Block(mrz)) {
                        mrz
                    } else {
                        null
                    }

                }
                // TD3
                88 -> {
                    if (!supportTD3) {
                        return null
                    }
                    val mrz = ocrf.fixTD3(block)
                    // handle validation checker
                    return if (mrzv.validateTD3Block(mrz)) {
                        mrz
                    } else {
                        null
                    }

                }
                // TD2
                72 -> {
                    if (!supportTD2) {
                        return null
                    }
                    val mrz = ocrf.fixTD2(block)
                    // handle validation checker
                    return if (mrzv.validateTD2Block(mrz)) {
                        mrz
                    } else {
                        null
                    }

                }

                in 0..71 -> {
                    Log.w(TAG, "Rejected Block [$block] because of Length")
                    return null
                }

                in 72..100 -> {
                    //handle mrz fixer
                    MRZ = stripWhiteSpace(block)
                    if (MRZ.length != 90 || MRZ.length != 88 || MRZ.length != 72 ) {
                        Log.w(
                            TAG,
                            "Rejected Block [$block] because of Inconsistent Length " + MRZ.length
                        )
                        return null
                    }

                    when (MRZ.length) {
                        90 -> {
                            MRZ = ocrf.fixTD1(MRZ)
                            // handle validation checker
                            return if (mrzv.validateTD1Block(MRZ)) {
                                MRZ
                            } else {
                                Log.w(TAG, "Rejected MRZ [$MRZ] because of Invalid MRZ")
                                null
                            }
                        }
                        88 -> {
                            MRZ = ocrf.fixTD3(MRZ)
                            // handle validation checker
                            return if (mrzv.validateTD3Block(MRZ)) {
                                MRZ
                            } else {
                                Log.w(TAG, "Rejected MRZ [$MRZ] because of Invalid MRZ")
                                null
                            }
                        }
                        72 -> {
                            MRZ = ocrf.fixTD2(MRZ)
                            // handle validation checker
                            return if (mrzv.validateTD2Block(MRZ)) {
                                MRZ
                            } else {
                                Log.w(TAG, "Rejected MRZ [$MRZ] because of Invalid MRZ")
                                null
                            }
                        }
                        else -> return null
                    }

                }
            }
        } else {
            return null
        }

        return MRZ
    }



}