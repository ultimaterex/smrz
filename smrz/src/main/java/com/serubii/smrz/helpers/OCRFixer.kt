package com.serubii.smrz.helpers

import com.serubii.smrz.others.countChar
import com.serubii.smrz.others.countString

/**
 *  that fixes certain inconsistencies with using firebase OCR
 *
 * Yes, this class is magic garbage
 *
 * @author Rex Serubii
 */
class OCRFixer {

    /**
     *  that fixes certain inconsistencies with using firebase OCR
     *
     * Yes, this function is magic garbage
     *
     * @param MRZ [String] you want to fix
     *
     * @return [String] that has been fixed
     *
     * @author Rex Serubii
     */
    internal fun fixTD1(MRZ: String): String {
        // firebase OCR seems to think DOC Number 0 is O
        val cutString = MRZ.substring(5, 14).replace("O", "0")

        var fixedString = cutString + MRZ.substring(14)

        // firebase sometimes sees < as k we replace <k< with <
        fixedString = fixedString.replace(("([<])([a-z])([<])").toRegex(), "<<<")

        // this is reaching a bit, but on position 18(17) sometimes 0 gets replaced by O
        // let's use REGEX to solve this problem for FRO0 to FR00
        fixedString = fixedString.replace(("(F)(R)([O])(0)").toRegex(), "FR00")


        return fixedString
    }

    /**
     *  that fixes certain inconsistencies with using firebase OCR
     *
     * Yes, this function is magic garbage
     *
     * @param MRZ [String] you want to fix
     *
     * @return [String] that has been fixed
     *
     * @author Rex Serubii
     */
    internal fun fixTD2(MRZ: String): String {

        // firebase OCR seems to think DOC Number 0 is O
        val cutString = MRZ.substring(36, 45).replace("O", "0")

        var fixedString = cutString + MRZ.substring(45)

        // firebase sometimes sees < as k we replace <k< with <
        fixedString = fixedString.replace(("([<])([a-z])([<])").toRegex(), "<<<")

        return fixedString
    }

    /**
     *  that fixes certain inconsistencies with using firebase OCR
     *
     * Yes, this function is magic garbage
     *
     * @param MRZ [String] you want to fix
     *
     * @return [String] that has been fixed
     *
     * @author Rex Serubii
     */
    internal fun fixTD3(MRZ: String): String {
// firebase OCR seems to think DOC Number 0 is O
        val cutString = MRZ.substring(44, 53).replace("O", "0")

        var fixedString = cutString + MRZ.substring(53)

        // firebase sometimes sees < as k we replace <k< with <
        fixedString = fixedString.replace(("([<])([a-z])([<])").toRegex(), "<<<")


        return fixedString
    }

    /** Expermental Fix that sometimes will turn « to < or << depending on the context
     *
     * @param MRZ String
     * @return String
     */
    internal fun fixExperimental(MRZ: String): String {

        val illegalCharCount = countString(MRZ, "«")

        val double = (illegalCharCount + MRZ.length)
        val single = MRZ.length

        var toReplaceWith = "<"

        when (double) {
            90 -> toReplaceWith = "<<"
            88 -> toReplaceWith = "<<"
            72 -> toReplaceWith = "<<"
        }

        when (single) {
            90 -> toReplaceWith = "<"
            88 -> toReplaceWith = "<"
            72 -> toReplaceWith = "<"
        }

        // firebase sometimes sees < as « we replace « with < or <<
        return MRZ.replace("«", toReplaceWith)
    }
}