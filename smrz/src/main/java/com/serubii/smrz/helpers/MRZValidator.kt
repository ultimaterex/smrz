package com.serubii.smrz.helpers

import com.serubii.smrz.others.containsDigit

class MRZValidator {

    /**
     *  validates MRZ TD1 (ID)
     *
     * We check if the size is 90,
     * the initial character is I; A or C,
     * the character at end of line 2 is a check digit(number)
     * the character at start of line is a letter
     *
     * @param MRZ [String] to validate
     *
     * @return [Boolean] true if valid, false otherwise
     *
     * @author Rex Serubii
     */
    internal fun validateTD1Block(MRZ: String): Boolean {

        // check size
        if (MRZ.length != 90) {
            return false
        }

        // check for at pos 1 for I, A or C
        if (!(MRZ.startsWith("I") || MRZ.startsWith("A") || MRZ.startsWith("C"))) {
            return false
        }

        // check for digit at position 60(59) is a number
        if (!containsDigit(MRZ[59].toString())) {
            return false
        }

        // check for digit at position 61(60) is a letter
        if (containsDigit(MRZ[60].toString())) {
            return false
        }


        return true
    }

    /**
     *  validates MRZ TD2 (Visas)
     *
     * We check if the size is 72,
     * the initial character is V
     * the character at position 10 of line 2 is a check digit(number)
     * the character at position 20 of line 2 is a check digit(number)
     *
     *
     * @param MRZ [String] to validate
     *
     * @return [Boolean] true if valid, false otherwise
     *
     * @author Rex Serubii
     */
    internal fun validateTD2Block(MRZ: String): Boolean {

        // check size
        if (MRZ.length != 72) {
            return false
        }

        // check for at pos 1 for V
        if (!MRZ.startsWith("V")) {
            return false
        }

        // check for digit at position 46(45) is a number
        if (!containsDigit(MRZ[45].toString())) {
            return false
        }

        // check for digit at position 56(55) is a letter
        if (containsDigit(MRZ[55].toString())) {
            return false
        }


        return true
    }

    /**
     *  validates MRZ TD3 (passport)
     *
     * We check if the size is 88,
     * the initial character is P
     * the character at pos 10 of line 2 is a check digit(number)
     * the character at pos 20 of line 2 is a check digit(number)
     * the character at pos 28 of line 2 is a check digit(number)
     * the character at pos 43 of line 2 is a check digit(number)
     * the character at pos 44 of line 2 is a check digit(number)
     *
     * @param MRZ [String] to validate
     *
     * @return [Boolean] true if valid, false otherwise
     *
     * @author Rex Serubii
     */
    internal fun validateTD3Block(MRZ: String): Boolean {

        // check size
        if (MRZ.length != 88) {
            return false
        }

        // check for at pos 1 for P
        if (!MRZ.startsWith("P")) {
            return false
        }

        // check for digit at position 54(53) is a number
        if (!containsDigit(MRZ[53].toString())) {
            return false
        }

        // check for digit at position 64(63) is a letter
        if (containsDigit(MRZ[63].toString())) {
            return false
        }

        // check for digit at position 72(71) is a letter
        if (containsDigit(MRZ[71].toString())) {
            return false
        }

        // check for digit at position 87(86) is a letter
        if (containsDigit(MRZ[86].toString())) {
            return false
        }

        // check for digit at position 88(87) is a letter
        if (containsDigit(MRZ[87].toString())) {
            return false
        }

        return true
    }

}