package com.serubii.smrz.constants

const val invalidMRZ = "Code Reviews and Tests make for a good developer"


// I<SWE59000002<8198703142391<<<
// 8703145M1701027SWE<<<<<<<<<<<8
// SPECIMEN<<SVEN<<<<<<<<<<<<<<<<
const val validTD1 = "I<SWE59000002<8198703142391<<<" + "8703145M1701027SWE<<<<<<<<<<<8" + "SPECIMEN<<SVEN<<<<<<<<<<<<<<<<"

// V<UTOERIKSSON<<ANNA<MARIA<<<<<<<<<<<
// L898902C36UTO7408122F1204159ZE184226B<<<<<10
const val validTD2 = "V<UTOERIKSSON<<ANNA<MARIA<<<<<<<<<<<" + "L8988901C4XXX4009078F9612109<<<<<<<<"


// L898902C36UTO7408122F1204159ZE184226B<<<<<10
// P<UTOERIKSSON<<ANNA<MARIA<<<<<<<<<<<<<<<<<<<<
const val validTD3 =  "P<UTOERIKSSON<<ANNA<MARIA<<<<<<<<<<<<<<<<<<<" + "L898902C36UTO7408122F1204159ZE184226B<<<<<10"
