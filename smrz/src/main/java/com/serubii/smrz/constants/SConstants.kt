package com.serubii.smrz.constants

const val smrz_id_type: String = "ID_TYPE"
const val smrz_issue_country: String = "ISSUE_COUNTRY"
const val smrz_document_number: String = "DOCUMENT_NUMBER"
const val smrz_expiration: String = "EXPIRATION"
const val smrz_surname: String = "SURNAME"
const val smrz_given_name: String = "GIVEN_NAME"
const val smrz_gender: String = "GENDER"
const val smrz_dob: String = "DOB"
const val smrz_nationality: String = "NATIONALITY"
const val smrz_optional_info: String = "OPTIONAL_INFO"
const val smrz_mrz_code: String = "MRZ"
const val smrz_photo_string: String = "PHOTO"
const val smrz_photo_front_string: String = "PHOTO_FRONT"
const val smrz_photo_back_string: String = "PHOTO_BACK"
const val smrz_photo_face_string: String = "PHOTO_FACET"

