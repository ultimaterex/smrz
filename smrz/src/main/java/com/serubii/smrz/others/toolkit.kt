package com.serubii.smrz.others

import android.util.Log

val TAG : String = "SMRZ Toolkit"

/**
 *   checks if [String] contains a digit of type [Int]
 *
 * @param string [String] to validate
 *
 * @return [Boolean] true if contains digit, false otherwise
 *
 * @author Rex Serubii
 */
internal fun containsDigit(string: String?): Boolean {
    var containsDigit = false
    if (string != null && string.isNotEmpty()) {
        for (c in string.toCharArray()) {
            if (Character.isDigit(c).also { containsDigit = it }) {
                break
            }
        }
    }
    println("Checked If Digit in $string with result $containsDigit")
    return containsDigit
}


/**
 *  that Strips and removes any whitespace, this includes spaces and linebreaks
 *
 * @param MRZ A [String] you want to strip from.
 *
 * @return [String] that has been stripped.
 *
 * @author Rex Serubii
 */
internal fun stripWhiteSpace(MRZ: String): String {
    var fixedString = MRZ.replace("\n", "")
    fixedString = fixedString.replace(" ", "")
    return fixedString
}


/** Returns the occurrences of a character in a string
 *
 * @param text String  to search through
 * @param filter Char to search for (keep to 1)
 * @return Int times found
 */
internal fun countChar(text: String, filter: Char ): Int {
    val occurrences = text.filter{ it in text}
        .groupingBy { it }
        .eachCount()

    return occurrences[filter]?: 0
}

/** Returns the occurences of a string in a string
 *
 * @param text String  to search through
 * @param filter String to search for (keep to 1)
 * @return Int times found
 */
internal fun countString(text: String, filter: String ): Int {
    return filter.count{ text.contains(it) }
}
